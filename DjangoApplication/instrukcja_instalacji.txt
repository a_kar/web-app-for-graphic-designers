Instrukcja instalacji aplikacji w systemie Windows

Wymagana jest wcześniejsza instalacja języka Python w wersji 3.8.1 lub wyższej.

1. Należy uruchomić wiersz polecenia i przejść do katalogu z plikami źródłowymi aplikacji.
Można to zrobić, np. używając poniższego polecenia:
cd <nazwa katalogu>

2. Następnie należy wybrać i aktykować środowisko wirtualne wpisując polecenie: 
myvenv\Scripts\activate

3. Należy zainstalować wymagane rozszerzenia i biblioteki.
Można to zrobić za pomocą polecenia:
pip install -r requirements.txt

4. By uruchomić serwer deweloperski, użyj polecenia:
python manage.py runserver

5. Otwórz adres http://127.0.0.1:8000/ (localhost:8000) w przeglądarce, by wyświetlić aplikację.

Logowanie do systemu

Jeśli aplikacja jest otwarta w przeglądarce można:
- zarejestrować się jako nowy użytkownik

- lub skorzystać z istniejących kont użytkownków:

konto administratora - login: admin; haslo: Haslo1234
konta pozostałych użytkowników - loginy do wyboru: anna, asia, janek, janusz, milena; haslo: Haslo1234

